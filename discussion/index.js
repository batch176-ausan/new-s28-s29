//[OBJECTIVE] Create a server-side app using Express
//Express Web Framework.


//Relate this task to something that you do on a daily basis.

//[SECTION]append the entire app to our node package manager.

//package.json -> the "heart" of every node project.
//this also contains different metadata that describes the structures 
//of the projects

//script -> is used to be declare and describe custom command and keyword
//that can be used to execute this project with the correct runtime environement

//NOTE: "start" is globally recognize amongst node projects and frameworks
//as the 'default' command script to execute a task/project
//however for *unconventional* keywords or command you have to append
// the command "run"
//SYNTAX: npm run <custom command>

//1. Identify and prepare the ingredients.
 const express = require("express");
//express => will be used as the main component to create the server.

//we need to be able to gather/acquire the ulitilities and components
//needed that the express library will provide us.
 //=> require () -> directive used to get the lbrary/components need inside module


//[SECTION] Create a Runtime environment that automatically autofix all 
//the changes in our app.

//were going to use a utility called nodemon.
//upon starting the entry point module with nodemon
//you will be able to the 'append' the application with the proper 
//run time environment. allowing you to save time and effort upon
//commiting changes to your app.

console.log(`Welcome to our Express API Server 
┼┼┼┼┼┼┼████
┼┼┼┼┼┼██████
┼┼┼┼┼████████
┼┼┼┼┼████████
┼┼┼┼┼█████░░░
┼┼┼┼┼████░░░░░░
┼┼┼┼┼┼██░░░░░░░░░
┼┼┼┼┼┼┼░░░░░░░░░░░
┼┼┼┼┼┼░░░░░░░░░░███
┼┼┼┼┼┼░░░░░░░░░█████▒▒▒
┼┼┼┼┼░░░░░░░███████▒▒▒▒▒
┼▒▒▒█░░░░░████████▒▒░░░▒▒
▒▒▒▒██████████████▒░░░░░▒
▒▒░░████████████▒▒▒▒░░░░▒
▒░░░█████████▒▒▒▒▒▒▒▒░░▒▒
▒░░░▒██████▒▒▒▒▒▒▒▒▒▒▒▒▒
▒▒░▒▒▒▒▒░░▒▒▒▒░░▒▒▒▒▒▒▒
┼▒▒▒▒▒▒░██▒▒▒▒██░▒▒▒▒
┼┼┼▒▒▒▒░██▒▒▒▒██░▒▒▒▒
┼┼┼▒▒▒▒▒▒▒░██░▒▒▒▒▒▒▒
┼┼┼▒▒▒▒▒▒░░██░░▒▒▒▒▒▒
┼┼┼┼▒▒▒▒░░░░░░░░▒▒▒▒
┼┼┼┼▒▒▒▒░░░░░░░░▒▒▒▒
┼┼┼┼┼▒▒▒░░░██░░░▒▒▒
┼┼┼┼┼┼▒▒▒░░░░░░▒▒▒
┼┼┼┼██░░▒▒▒▒▒▒▒▒░░██
┼┼███░░░████████░░░███
┼████░░██████████░░████
┼████░░░░░░░░░░░░░░████
█████░░░░░░░░░░░░░░█████
████▒▒░░░░░░░░░░░░▒▒████
██▒▒▒▒▒██████████▒▒▒▒▒██
█▒▒▒▒▒▒▒████████▒▒▒▒▒▒▒█
▒▒▒▒▒▒▒▒████████▒▒▒▒▒▒▒▒
▒▒▒▒▒▒▒▒████████▒▒▒▒▒▒▒▒
▒▒▒▒▒▒▒▒████████▒▒▒▒▒▒▒▒
┼▒▒▒▒▒▒░████████░▒▒▒▒▒▒
┼┼▒▒▒▒░░░██████░░░▒▒▒▒
┼┼┼┼░░░░░░░░░░░░░░░░░
┼┼┼┼┼░░░░░░░░░░░░░░░
┼┼┼┼┼░░░░░░░░░░░░░░░
┼┼┼┼┼┼░░░░░░░░░░░░░
┼┼┼┼┼┼░░░░░░░░░░░░░
┼┼┼┼░░░░░░░┼┼░░░░░░░
┼┼┼░░░░░░░░┼┼░░░░░░░░
┼┼░░░░░░░░░┼┼░░░░░░░░░
┼┼░░░░░░░░░┼┼░░░░░░░░░
┼┼┼░░░░░░░░┼┼░░░░░░░░
┼┼┼┼███░░░┼┼┼┼░░░███
┼┼████████┼┼┼┼████████
┼█████████┼┼┼┼█████████
██████████┼┼┼┼██████████
██████████┼┼┼┼██████████
┼████████┼┼┼┼┼┼████████
`)

//you can even insert items like text art into your 